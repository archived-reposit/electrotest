#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include "libresistance.h"

float calc_resistance(int number_of_components, char connection_type, float *component_values)
{
  /* Från https://www.moodle.umu.se/mod/assign/view.php?id=142952 */
  /* count: Antal komponenter. */
  /* conn: Seriellt eller parallellt kopplade komponeter [ P | S ]. */
  /* *array: En pekare på en array av komponentvärden som är lika stor som count. */
  /* Värdet 0 skall returneras om något motstånd är noll vid parallellkoppling, dvs  R1||R2=0, om R1 eller R2 är 0Ohm. */
  /* Biblioteket får inte krascha om en "nollpekare" skickas till funktioen, dvs om array=0. */
  /* Om argumenten är felaktiga skall funktionen returnera -1. */
  /* Returvärdet är den resulterande resistansen. */
  float resistance = 0;
  /* printf("Count: %d\n", number_of_components); */
  /* printf("Connection: %c\n", conn); */
  // number_of_components = 3; // todo: remove
  
  int i;
  switch(connection_type) {
  case 'S' : // seriell koppling
      for (i=0; i<number_of_components; i++){
	resistance += component_values[i];
      }
      break;
      case 'P' : // paralell koppling
      for (i=0; i<number_of_components; i++){
	resistance += 1 / component_values[i];
      }
    }
	printf("Returning: %f\n", resistance );  
  //todo:felhanteringen
  return resistance;
}
