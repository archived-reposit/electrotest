//
// Created by li on 2019-09-22.
//
#include <stdio.h>
#include "libcomponent.h"

//E12 serien mellan 10 och 100

void e_resistance(float orig_resistance, float *res_array)
{
    float resistance = orig_resistance;
    int utIndex = 0;
    //orig_resistance är ersättningsresistansen
    for (int i = 0; i < sizeof(e12); i++)
    {
        int multiplier = getMultiplier(resistance);
        if (e12[i]*multiplier >= resistance)
        {
            resistance = resistance - e12[i-1];
            res_array[utIndex] = e12[i-1];
            utIndex++;
        } else if (e12[i]*multiplier == resistance)
        {
            *res_array[utIndex] = e12[i];
            return;
        }
    }

}

int getMultiplier(float resistance)
{
    if (resistance < 10)
    {
        return 1;
    } else if (resistance < 100)
    {
        return 10;
    } else if (resistance < 1000)
    {
        return 100;
    } else if (resistance < 10000)
    {
        return 1000;
    }
}