#include <stdio.h>

#include "libpower.h"


//spänning(U) anges i volt, resistansen(R) anges i resistance
//P=U^2/R (spänning i kvadrat delat i resistansen)
float calc_power_r(float volt, float resistance)
{
   return (volt*volt)/resistance;
}

//strömmen anges i current
//P=U*I (spänning gånger strömmen)
float calc_power_i(float volt, float current)
{
   return volt * current;
}


