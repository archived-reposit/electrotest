//
// Created by li on 2019-09-22.
//

#ifndef ELECTROTEST_MASTER_LIBCOMPONENT_H
#define ELECTROTEST_MASTER_LIBCOMPONENT_H

#include <stdio.h>

//E12 serien grundvärden
float e12[12] = {1.0, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2};

/*
 * Beräknar vilka tre seriekopplade resistorer i E12-serien
 * som närmast ersätter den resistans som skickas med
 */
void e_resistance(float orig_resistance, float *res_array);

int getMultiplier(float resistance);

#endif //ELECTROTEST_MASTER_LIBCOMPONENT_H
