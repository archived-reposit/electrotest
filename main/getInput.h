/*
 * Datum: 2019-09-16
 * Program electrotest
 * File: getInput.h
 * Author: Richard Holm
 * Course: LinUM - 5EL142
 * Assignment: 6
 * Group: 192
 */

#ifndef GETINPUT_H_
#define GETINPUT_H_

typedef struct {
	double volt;
	char conn;  // Connection type
	int count;  // count of resistors
	char quit;
	float *array;
} Dataset;

float input_float();

void getInput_volt(char*, Dataset*);

void getInput_conn(char*, Dataset*);

void getInput_count(char*, Dataset*);

void getInput_components(Dataset*);

#endif /* GETINPUT_H_ */
