/*
 * Datum: 2019-09-16
 * Program electrotest
 * File: main.c
 * Author: Richard Holm
 * Course: LinUM - 5EL142
 * Assignment: 6
 * Group: 192
 */

#include <stdio.h>
#include <stdlib.h>
#include "getInput.h"

// Start of mockups
float calc_resistance(int, char, float*);
float calc_resistance(int count, char conn, float *array){return 0.0;}
float calc_power_r(float, float);
float calc_power_r(float volt, float resistance){return 0.0;}
float calc_power_i(float, float);
float calc_power_i(float volt, float current){return 0.0;}
int e_resistance(float, float*);
int e_resistance(float orig_resistance, float *res_array){return 0;}
// End of mockups


int main(void){

	//---- Hämta referensdata -------------------------------------

	Dataset refdata;  // Defined in getInput.h
	refdata.volt  = 0.0;
	refdata.conn  = '-';
	refdata.count = 0;
	refdata.array=NULL;
	refdata.quit = '-';

	printf("==== Electrotest, Assignment 6, Course LinUM - 5EL142, Group 192 ====\n");
	int EOI = 0;  // End Of Input, 0=false
	while(!EOI){

		getInput_volt("Ange spänningskälla i V: ", &refdata);
		if(refdata.quit=='Q')break;

		getInput_conn("Ange koppling[S | P]: ", &refdata);
		if(refdata.quit=='Q')break;

		getInput_count("Antal komponenter: ", &refdata);
		if(refdata.quit=='Q')break;

		if(refdata.count > 0)getInput_components(&refdata);
		if(refdata.quit=='Q')break;

		EOI = 1; // end of input
	}
	if(refdata.quit=='Q'){
		printf("\nProgram terminated.");
		exit(0);
	}

	//---- Presentera referensdata --------------------------------

	printf("\n");
	printf("------------------------------------------");
	printf("\n");
	//printf("Data enligt följande:\n");
	printf("Volt: %.2f\n", refdata.volt);
	printf("Kopplingstyp: %c\n", refdata.conn);
	printf("Antal komponenter: %d\n", refdata.count);
	for(int m=0; m<refdata.count;m++){
		printf("Resistor %d: %.1f\n", m+1, refdata.array[m]);
	}

	//---- Bearbeta referensdata ----------------------------------
	//==== Mockups ================================================

	// call to resistance calculation
	// float calc_resistance(int count, char conn, float *array);
	float resistance=0.0;
	resistance = calc_resistance(refdata.count, refdata.conn, refdata.array);

	// call to power calculation
	// float calc_power_r(float volt, float resistance);
	float power_r=0.0;
	power_r = calc_power_r(refdata.volt, resistance);

	// float calc_power_i(float volt, float current);
	float power_i=0.0;
	float current=0.0;
	power_i = calc_power_i(refdata.volt, current);

	// call to resistande choises
	// int count = e_resistance(float orig_resistance, float *res_array);
	int count;
	float array[3]={0.0, 0.0, 0.0};
	float *res_array=array;
	count = e_resistance(resistance, res_array);
	//=============================================================

	//---- Presentera bearbetad data ------------------------------

	printf("Ersättningsresistans: %.1f\n", resistance);
	printf("Effekt: %.2f, %.2f\n", power_r, power_i);
	printf("Ersättningsresistanser i E12-serien kopplade i serie:\n");
	count=3;
	for(int i=0;i<count;i++){
		printf("Resistor %d: %.1f Ohm\n", i+1, array[i]);
	}

	//---- Städa allokerad data -----------------------------------

	free(refdata.array);
	refdata.array = NULL;
	return(0);
}

