/*
 * Datum: 2019-09-16
 * Program electrotest
 * File: getInput.c
 * Author: Richard Holm
 * Course: LinUM - 5EL142
 * Assignment: 6
 * Group: 192
 */

#include <stdbool.h>  // bool
#include <stdio.h>    // EOF
#include <stdlib.h>
#include <ctype.h>  // isdigit, isspace
#include "getInput.h"

float input_float(){
/* Ref: https://stackoverflow.com/questions/37341693/
c-read-in-float-value-using-getchar-and-print-out-float-using-printf */
    float f = 0.;
    int c;
    bool initial = 1, final = 0;
    int pos = 0;
    float decimal = 0;

    while (((c = getchar()) != EOF) && (c != '\n')) {
	        pos += 1;
	        if (isspace(c)) {
	            if (initial || final) continue;
	            else {
	                final = 1;
	                continue;
	            }
	        }
	        else if (final) {
	            error(pos, c);
	        }

	        initial = 0;
	        if (c == '.') {
	            if (decimal) {
	                error(pos, c);
	            }
	            else decimal = 1;
	        }
	        else if (isdigit(c) == 0) {
	        	if(c=='q' || c=='Q'){
	        		return -1;
	        	}
	        }
	        else if (decimal == 0) {
	            f = f * 10 + c - '0';
	        }
	        else {
	            decimal *= .1;
	            f += (c - '0') * decimal;
	        }
	    }
	return f;
}

void getInput_volt(char *message, Dataset *ds){
	float t;

	printf("[Q=quit] %s", message);
	t = input_float();
	if(t==-1){
		ds->quit='Q';
		return; // exit the program
	 }
	 else ds->volt = t;
}

void getInput_conn(char *message, Dataset *ds){
	int c = 0x00;

	printf("[Q=quit] %s", message);

	while((c = getchar()) != '\n'){
		switch(c){
		case 'S':
		case 's': ds->conn = 'S'; break;
		case 'P':
		case 'p': ds->conn = 'P'; break;
		case 'q':
		case 'Q': ds->quit = 'Q'; break;  // Quit
		default: ds->quit = 'Q';
		}
	}
}

void getInput_count(char *message, Dataset *ds){
	int c=0;

	printf("[Q=quit 0-9] %s", message);

	while(((c = getchar()) != EOF) && (c != '\n')){
		if(c == 0x0A)ds->quit='Q'; //ENTER KEY is pressed
		else if(c=='q' || c=='Q')ds->quit='Q';
		else if(c > 47 && c < 58)ds->count = c-48;
	}
}

void getInput_components(Dataset *ds){
	float t;
	int initialSize=1;
	ds->array = (float*)malloc(initialSize*sizeof(float));

	if(ds->array == NULL){ // Test if out of heap
	    printf("Memory allocation failed");
	    ds->quit='Q';
	    return; // exit the program
	}

	for(int n=0;n<ds->count;n++){
		printf("[Q=quit] Komponent %d i ohm:", n+1);
		t = input_float();
		if(t==-1){
			ds->quit='Q';
			return; // exit the program
		 }
		 else ds->array[n] = t;
	}
}
